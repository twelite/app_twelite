#!/bin/sh

GITLABUSER=`echo $USER|tr -d '-'`

TAG=16.04_201805
docker pull ubuntu:16.04
docker build --rm -f Dockerfile.xenial -t $GITLABUSER/mwsdk:$TAG .

TAG=18.04_201805
docker pull ubuntu:18.04
docker build --rm -f Dockerfile.bionic -t $GITLABUSER/mwsdk:$TAG .
docker tag $GITLABUSER/mwsdk:$TAG $GITLABUSER/mwsdk
